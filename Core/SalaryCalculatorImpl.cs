﻿namespace Core
{
    public class HourlyCalculator : ISalaryCalculator
    {
        internal HourlyCalculator() { }
        public double Calculate(EmployeeDTO dto) => 120 * dto.HourlySalary * 12;
    }

    public class MonthlyCalculator : ISalaryCalculator
    {
        internal MonthlyCalculator() { }
        public double Calculate(EmployeeDTO dto) => dto.MonthlySalary * 12;
    }

}
