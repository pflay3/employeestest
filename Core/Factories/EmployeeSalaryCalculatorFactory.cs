﻿using System;

namespace Core
{
    public class EmployeeSalaryCalculatorFactory
    {
        public ISalaryCalculator Build(string contractType)
        {
            switch (contractType?.ToUpper())
            {
                case "HOURLYSALARYEMPLOYEE": return new HourlyCalculator();
                case "MONTHLYSALARYEMPLOYEE": return new MonthlyCalculator();
                default:
                    throw new Exception($"Unknown Contract Type: {contractType}");
            }
        }
    }
}
