﻿namespace Core
{
    public interface ISalaryCalculator
    {
        double Calculate(EmployeeDTO dto);
    }
}
