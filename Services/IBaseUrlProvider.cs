﻿namespace Services
{
    public interface IBaseUrlProvider
    {
        string BaseUrl { get; }
    }
}
