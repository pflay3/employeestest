﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core;

namespace Services
{
    public interface IEmployeeService
    {
        Task<IEnumerable<EmployeeDTO>> GetAllEmployees();
        Task<EmployeeDTO> GetSingleEmployee(int employeeId);
    }
}
