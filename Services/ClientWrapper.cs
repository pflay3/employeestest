﻿using EmployeeManager.ExternalWrapper;

namespace Services
{
    public class ClientWrapper
    {
        private readonly IBaseUrlProvider _baseUrlProvider;

        public ClientWrapper(IBaseUrlProvider baseUrlProvider)
        {
            _baseUrlProvider = baseUrlProvider;
        }

        public EmployeeWebClient GetWebClient()
        {
            return new EmployeeWebClient
            {
                BaseUrl = _baseUrlProvider.BaseUrl
            };
        }
    }
}
