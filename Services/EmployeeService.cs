﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Core;
using EmployeeManager.ExternalWrapper;

namespace Services
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IMapper _mapper;
        private readonly EmployeeSalaryCalculatorFactory _factory;
        private readonly ClientWrapper _clientWrapper;

        public EmployeeService(
            EmployeeSalaryCalculatorFactory factory,
            ClientWrapper clientWrapper,
            IMapper mapper)
        {
            _mapper = mapper;
            _factory = factory;
            _clientWrapper = clientWrapper;
        }

        public async Task<IEnumerable<EmployeeDTO>> GetAllEmployees()
        {
            var employeeList = await _clientWrapper
                .GetWebClient()
                .ApiEmployeesGetAsync();

            return employeeList.Select(Transform);
        }

        public async Task<EmployeeDTO> GetSingleEmployee(int employeeId)
        {
            var employeeList = await _clientWrapper
                .GetWebClient()
                .ApiEmployeesGetAsync();

            var employee = employeeList.FirstOrDefault(e => e.Id == employeeId);
            return Transform(employee);
        }

        private EmployeeDTO Transform(Employee employee)
        {
            var dto = _mapper.Map<EmployeeDTO>(employee);

            var calculator = _factory.Build(dto.ContractType);
            dto.AnnualSalary = calculator.Calculate(dto);
            return dto;
        }
    }
}
