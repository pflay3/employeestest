﻿using System.Collections.Generic;
using System.Threading.Tasks;
using API.Controllers.Models;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IEmployeeService _employeeService;

        public EmployeesController(
            IEmployeeService employeeService,
            IMapper mapper
            )
        {
            _mapper = mapper;
            _employeeService = employeeService;
        }

        [HttpGet]
        [Route("{employeeId}")]
        [ProducesResponseType(typeof(EmployeeModel), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetSingle(int employeeId)
        {
            var res = await _employeeService.GetSingleEmployee(employeeId);
            return Ok(_mapper.Map<EmployeeModel>(res));
        }

        [HttpGet]
        [ProducesResponseType(typeof(List<EmployeeModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAll()
        {
            var res = await _employeeService.GetAllEmployees();
            return Ok(_mapper.Map<List<EmployeeModel>>(res));
        }
    }
}