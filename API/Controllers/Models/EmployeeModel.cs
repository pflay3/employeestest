﻿namespace API.Controllers.Models
{
    public class EmployeeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double AnnualSalary { get; set; }
        public string Role { get; set; }
        public string ContractType { get; set; }
    }
}
