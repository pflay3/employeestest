﻿using API.Infrastructure;
using AutoMapper;
using Core;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Services;

namespace API
{
    public class Startup
    {
        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(c =>
            {
                c.AddPolicy(MyAllowSpecificOrigins, options => options.AllowAnyMethod().AllowAnyHeader().AllowAnyOrigin());
            });

            services.AddAutoMapper(typeof(Startup));
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.Configure<AppConfig>(Configuration.GetSection("AppConfig"));

            services.AddScoped<IBaseUrlProvider, BaseUrlProvider>();

            services.AddScoped<EmployeeSalaryCalculatorFactory, EmployeeSalaryCalculatorFactory>();
            services.AddScoped<IEmployeeService, EmployeeService>();
            services.AddScoped<ClientWrapper, ClientWrapper>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(MyAllowSpecificOrigins);
            app.UseMvc();
        }
    }
}
