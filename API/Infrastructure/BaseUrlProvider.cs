﻿using System;
using Microsoft.Extensions.Options;
using Services;

namespace API.Infrastructure
{
    public class BaseUrlProvider : IBaseUrlProvider
    {
        private readonly IOptions<AppConfig> _appConfig;

        public BaseUrlProvider(IOptions<AppConfig> appConfig)
        {
            _appConfig = appConfig;
        }

        public string BaseUrl { get { return _appConfig.Value.BaseUrl; } }
    }
}
