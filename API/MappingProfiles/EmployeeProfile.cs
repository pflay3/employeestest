﻿using API.Controllers.Models;
using AutoMapper;
using Core;
using EmployeeManager.ExternalWrapper;

namespace API.MappingProfiles
{
    public class EmployeeProfile : Profile
    {
        public EmployeeProfile()
        {
            CreateMap<EmployeeDTO, EmployeeModel>();

            CreateMap<Employee, EmployeeDTO>()
                .ForMember(d => d.ContractType, o => o.MapFrom(s => s.ContractTypeName))
                .ForMember(d => d.Role, o => o.MapFrom(s => s.RoleName));
        }
    }
}
