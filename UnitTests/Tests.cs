using AutoMapper;
using Core;
using EmployeeManager.ExternalWrapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Services;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;

namespace UnitTests
{
    [TestClass]
    public class Tests
    {
        IEmployeeService employeeService;

        public Tests()
        {
            employeeService = new EmployeeService(new Core.EmployeeSalaryCalculatorFactory(),
                                    new ClientWrapper(new BaseUrlProvider()), GetMapper());
        }

        public IMapper GetMapper()
        {
            var config = new MapperConfiguration(opts =>
            {
                opts.CreateMap<Employee, EmployeeDTO>()
                .ForMember(d => d.ContractType, o => o.MapFrom(s => s.ContractTypeName))
                .ForMember(d => d.Role, o => o.MapFrom(s => s.RoleName));
            });

            return config.CreateMapper();
        }

        [TestMethod]
        public async Task TestGetAllEmployees()
        {
            var res =  await employeeService.GetAllEmployees();
            List<EmployeeDTO> results = new List<EmployeeDTO>(res);
            Assert.AreEqual(2, results.Count);
        }

        [TestMethod]
        public async Task TestGetEmployee1()
        {
            var res = await employeeService.GetSingleEmployee(1);
            Assert.AreEqual(1, res.Id);
        }

        [TestMethod]
        public async Task TestGetEmployee2()
        {
            var res = await employeeService.GetSingleEmployee(2);
            Assert.AreEqual(2, res.Id);
        }

        [TestMethod]
        public void TestCalculatorHour()
        {
            EmployeeSalaryCalculatorFactory factory = new EmployeeSalaryCalculatorFactory();
            var calculator = factory.Build("HOURLYSALARYEMPLOYEE");
            var result = calculator.Calculate(new EmployeeDTO { HourlySalary = 10 });

            Assert.AreEqual(14400, result);
        }

        [TestMethod]
        public void TestCalculatorMonth()
        {
            EmployeeSalaryCalculatorFactory factory = new EmployeeSalaryCalculatorFactory();
            var calculator = factory.Build("MONTHLYSALARYEMPLOYEE");
            var result = calculator.Calculate(new EmployeeDTO { MonthlySalary = 10 });

            Assert.AreEqual(120, result);
        }
    }


    public class BaseUrlProvider : IBaseUrlProvider
    {
        public string BaseUrl { get { return "http://masglobaltestapi.azurewebsites.net"; } }
    }
}
