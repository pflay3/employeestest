# Employees Test

Web app to get data from Api Rest


# Projects

## Core

Contains business logic classes to calculate salary

## Services

Provide classes to connect external api and apply business rules

## ExternalWrapper

Generated Proxy to connect with api http://masglobaltestapi.azurewebsites.net. To generate proxy use [https://github.com/RicoSuter/NSwag](https://github.com/RicoSuter/NSwag)

## API

Api Rest to expose methods to get data

## WebApp

MVC App to show data in html table

## UnitTests

Project to test Core and Services
